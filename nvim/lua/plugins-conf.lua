local config = {}

-- nvim-cmp --------------------------------------------------------------{{{
config['cmp'] = function()
	vim.cmd('set completeopt=menu,menuone,noselect')
	local cmp = require 'cmp'
	local cmp_ultisnips_mappings = require("cmp_nvim_ultisnips.mappings")

	cmp.setup({
		snippet = {
			expand = function(args)
				vim.fn["UltiSnips#Anon"](args.body)
			end,
		},
		window = {
			completion = cmp.config.window.bordered(),
			documentation = cmp.config.window.bordered(),
			-- winhighlight = "Normal:Pmenu,FloatBorder:Pmenu,Search:None",
		},
		mapping = cmp.mapping.preset.insert({
			['<C-b>'] = cmp.mapping.scroll_docs(-4),
			['<C-f>'] = cmp.mapping.scroll_docs(4),
			['<C-Space>'] = cmp.mapping.complete(),
			['<C-e>'] = cmp.mapping.abort(),
			['<Tab>'] = cmp.mapping.confirm({ select = true }),
			['<A-n>'] = cmp.mapping(
				function(fallback)
					cmp_ultisnips_mappings.compose { "jump_forwards" } (fallback)
				end,
				{ "i", "s", }
			),
			['<A-p>'] = cmp.mapping(
				function(fallback)
					cmp_ultisnips_mappings.jump_backwards(fallback)
				end,
				{ "i", "s", }
			),
		}),
		sources = cmp.config.sources({
			{ name = 'nvim_lsp' },
			{ name = 'ultisnips' },
			{ name = 'path' },
		}, {
			{ name = 'buffer' },
		}),
		formatting = {
			format = require('lspkind').cmp_format(),
		},
	})

	cmp.setup.filetype('gitcommit', {
		sources = cmp.config.sources({
			{ name = 'cmp_git' },
		}, {
			{ name = 'buffer' },
		})
	})

	cmp.setup.cmdline({ '/', '?' }, {
		mapping = cmp.mapping.preset.cmdline(),
		sources = {
			{ name = 'buffer' }
		}
	})

	cmp.setup.cmdline(':', {
		mapping = cmp.mapping.preset.cmdline(),
		sources = cmp.config.sources({
			{ name = 'path' }
		}, {
			{ name = 'cmdline' }
		})
	})

	vim.api.nvim_set_hl(0, "CmpItemAbbrMatch", { link = "Function" })
	vim.api.nvim_set_hl(0, "CmpItemAbbrMatchFuzzy", { link = "Identifier" })
end

config['inlayhints'] = function()
	require("lsp-inlayhints").setup()
	vim.api.nvim_create_autocmd("LspAttach", {
		group = vim.api.nvim_create_augroup("LspAttach_inlayhints", {}),
		callback = function(args)
			if not (args.data and args.data.client_id) then
				return
			end

			local bufnr = args.buf
			local client = vim.lsp.get_client_by_id(args.data.client_id)
			require("lsp-inlayhints").on_attach(client, bufnr)
		end,
	})
end

config['git-signs'] = function()
	local sign = function(opts)
		vim.fn.sign_define(opts.name, {
			texthl = opts.name,
			text = opts.text,
			numhl = ''
		})
	end

	sign({ name = 'DiagnosticSignError', text = '✘' })
	sign({ name = 'DiagnosticSignWarn', text = '▲' })
	sign({ name = 'DiagnosticSignHint', text = '⚑' })
	sign({ name = 'DiagnosticSignInfo', text = '' })

	require('gitsigns').setup()
end

-- telescope ------------------------------------------------------------
config['telescope'] = function()
	require('project_nvim').setup()
	require('telescope').setup {
		defaults = {
			history = false
		}
	}
	require('telescope').load_extension('projects')
end
-- }}}

-- lualine --------------------------------------------------------------
config['lualine'] = function()
	require('lualine').setup {
		options = {
			globalstatus = true
		},
		sections = {
			lualine_c = {}
		},
		tabline = {
			lualine_a = { 'buffers' },
			lualine_z = { 'tabs' }
		},
		winbar = {
			lualine_a = {},
		},
		inactive_winbar = {
			lualine_a = {},
		},
	}
end

config['incline'] = function()
	require('incline').setup {
		hide = {
			cursorline = true,
			only_win = true,
		},
		render = function(props)
			local filename = vim.fn.fnamemodify(vim.api.nvim_buf_get_name(props.buf), ':t')
			local icon, color = require('nvim-web-devicons').get_icon_color(filename)
			return {
				{
					icon,
					guifg = color
				},
				{ ' ' .. filename },
			}
		end
	}
end

-- treesitter -----------------------------------------------------------
config['treesitter'] = function()
	require 'nvim-treesitter.configs'.setup {
		highlight = {
			enable = true,
			additional_vim_regex_highlighting = true,
		},

		incremental_selection = {
			enable = true,
			keymaps = {
				init_selection = "<Cr>",
				node_incremental = "<Cr>",
				scope_incremental = "<Leader><Cr>",
				node_decremental = "<BS>",
			},
		},

		indent = {
			enable = true,
			disable = { "fortran" },
		},
	}
	vim.cmd [[
	autocmd FileType markdown :TSBufDisable highlight
]]
end

config['treesitter-context'] = function()
	require('treesitter-context').setup {
		enable = true,
		throttle = true,
	}
end

-- nvim-autopairs -------------------------------------------------------
config['autopairs'] = function()
	require("nvim-autopairs").setup()
	local npairs = require 'nvim-autopairs'
	local Rule = require 'nvim-autopairs.rule'
	local cond = require 'nvim-autopairs.conds'

	npairs.add_rules {
		Rule('<', '>', { "vim" })
				:with_pair(cond.not_before_regex("^%s*if .*", 100)),
		Rule('<', '>', { "rust", "cpp" })
				:with_pair(cond.not_before_regex("[ <]"), 1),
		Rule('<', '>', { "html", "svelte" })
				:with_pair(cond.not_before_regex("[A-Za-z0-9]")),
		Rule('$', '$', { "tex", "markdown" }),

		-- space inside pairs
		Rule(' ', ' ')
				:with_pair(function(opts)
					local pair = opts.line:sub(opts.col - 1, opts.col)
					return vim.tbl_contains({ '()', '[]', '{}' }, pair)
				end),
		Rule('( ', ' )')
				:with_pair(function() return false end)
				:with_move(function(opts)
					return opts.prev_char:match('.%)') ~= nil
				end)
				:use_key(')'),
		Rule('{ ', ' }')
				:with_pair(function() return false end)
				:with_move(function(opts)
					return opts.prev_char:match('.%}') ~= nil
				end)
				:use_key('}'),
		Rule('[ ', ' ]')
				:with_pair(function() return false end)
				:with_move(function(opts)
					return opts.prev_char:match('.%]') ~= nil
				end)
				:use_key(']'),
	}
end

-- surround.nvim --------------------------------------------------------
config['surround'] = function()
	require 'surround'.setup {
		prefix = "s",
		mappings_style = "surround",
		map_insert_mode = false,
		pairs = {
			nestable = {
				p = { "(", ")" },
				b = { "[", "]" },
				B = { "{", "}" },
				a = { "<", ">" }
			},
			linear = {
				s = { ' ', ' ' },
				q = { "'", "'" },
				d = { '"', '"' },
				t = { '`', '`' },
				i = { '*', '*' },
				e = { '**', '**' },
				E = { '***', '***' },
				m = { '$', '$' },
				v = { '|', '|' },
			},
		},
	}
end

-- nvim-comment ---------------------------------------------------------
config['comment'] = function()
	require('Comment').setup {
		ignore = '^$',
		toggler = {
			line = '<C-c>',
			block = '<A-c>',
		},
		opleader = {
			line = '<C-c>',
			block = '<A-c>',
		},
	}
end

-- indent-blank-line ----------------------------------------------------
config['indent-blankline'] = function()
	require("ibl").setup({
		indent = {
			char = "▎",
			tab_char = "▎"
		},
		scope = {
			enabled = true,
			highlight = "Special",
		},
	})

	vim.cmd [[
	autocmd FileType help,qf,term,csv,alpha,fzf,coc-explorer :IBLDisable
]]
end

-- misc -----------------------------------------------------------------
config['catppuccin'] = function()
	vim.cmd.colorscheme 'catppuccin'
end

config['illuminate'] = function()
	vim.cmd [[
	highlight! link IlluminatedWordText CursorColumn
	highlight! link IlluminatedWordRead CursorColumn
	highlight! link IlluminatedWordWrite CursorColumn
	]]
end

config['nvim-colorizer'] = function()
	require 'colorizer'.setup {
		css = true,
		css_fn = true,
	}
end

config['hop'] = function()
	require('hop').setup()
	vim.keymap.set('n', '<Leader>w', ':HopChar1<Cr>', { noremap = true })
end

config['slime'] = function()
	if vim.fn.has('unix') then
		vim.keymap.set('n', '<F7>', ':bot 10new<Cr>:terminal<Cr>a', { noremap = true })
	else
		vim.keymap.set('n', '<F7>', ':bot 10new<Cr>:terminal powershell<Cr>a', { noremap = true })
	end

	vim.keymap.set('n', '<C-s>', '<Plug>SlimeLineSend')
	vim.keymap.set('n', '<A-s>', '<Plug>SlimeParagraphSend')
	vim.keymap.set('x', '<C-s>', '<Plug>SlimeRegionSend')
end

config['align'] = function()
	vim.keymap.set(
		'x',
		'aa',
		function()
			require'align'.align_to_char({
				length = 1,
			})
		end,
		{ noremap = true, silent = true }
	)

	vim.keymap.set(
		'x',
		'aw',
		function()
			require'align'.align_to_string({
				regex = false,
			})
		end,
		{ noremap = true, silent = true }
	)
end

config['copilot'] = function()
	require('copilot').setup({
		suggestion = {
			auto_trigger = true,
			keymap = {
				accept = "<M-Space>",
			}
		},
		panel = { enabled = false },
		filetypes = {
			markdown = false,
			yaml = true,
			help = false,
			gitcommit = false,
			gitrebase = false,
			svn = false,
			cvs = false,
			["."] = false,
		},
	})
end

-- languge servers -------------------------------------------------{{{
config['lspconfig'] = function()
	local lsp = require('lspconfig')

	lsp.rust_analyzer.setup {
		settings = {
			["rust-analyzer"] = {
				cargo = {
					features = "all"
				}
			}
		}
	}

	lsp.clangd.setup({
		capabilities = {
			offsetEncoding = 'utf-16'
		},
	})

	lsp.ltex.setup({
		settings = {
			ltex = {
				language = "auto"
			}
		},
	})

	lsp.pylsp.setup({
		settings = {
			pylsp = {
				plugins = {
					pycodestyle = {
						ignore = { "W191" }
					}
				}
			}
		},
	})

	lsp.html.setup({
		capabilities = {
			textDocument = {
				completion = {
					completionItem = {
						snippetSupport = true
					}
				}
			}
		},
		filetypes = { "html", "htmldjango" }
	})

	lsp.cssls.setup({
		capabilities = {
			textDocument = {
				completion = {
					completionItem = {
						snippetSupport = true
					}
				}
			}
		}
	})

	lsp.lua_ls.setup {}
	lsp.ts_ls.setup {}
	lsp.astro.setup {}
	lsp.fortls.setup {}
	lsp.nil_ls.setup {}
end
-- }}}

return config
