if not status is-interactive
	return
end

if test -f ~/.extra_source
	source ~/.extra_source
end

if status is-login
	if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
		XDG_CURRENT_DESKTOP=sway exec sway
		return
	end
end

if not set -q TMUX
	tmux -2 new-session -A -s main 2> /dev/null
end
# if not set -q ZELLIJ
# 	zellij attach -c main
# end

fish_vi_key_bindings

# alias
# ----------------------------------------------------------------------
# ls
if command -v eza >/dev/null
	alias ls='eza --group-directories-first'
else
	functions --copy ls __fish_ls
	alias ls='__fish_ls --group-directories-first'
end
alias l="ls -lhF"
alias la="ls -lAhF"
alias lt="ls -ThF"

# grep
alias grep="grep --color=auto"

if test $TERM = xterm-kitty
	alias diff="kitty +kitten diff"
	alias icat="kitty +kitten icat"
else
	alias diff="diff --color"
end

# math
abbr -a m qalc

# shortcuts
alias n="$FILEMANAGER"
alias mnv="mpv --no-video"
abbr -a mt trash
abbr -a cr "su -c 'sync; echo 3 > /proc/sys/vm/drop_caches'"

set -gx PATH $PATH ~/.cargo/bin ~/.shell

# tweaks
# ----------------------------------------------------------------------
alias !="disown"

# initializations
zoxide init fish | source
starship init fish | source
fzf --fish | source
~/Code/pay-respects/target/release/pay-respects fish --alias | source
