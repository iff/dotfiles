#!/bin/sh
if [[ -z $(pidof swaylock) ]]; then
	swayidle \
		timeout 10 'swaymsg "output * dpms off"' \
		resume 'swaymsg "output * dpms on"' &
	swaylock -F -i $HOME/Documents/Templates/wallpaper.jpg -s fill
	kill %%
fi
