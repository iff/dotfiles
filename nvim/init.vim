" NeoVim settings{{{
" ======================================================================

" Configs --------------------------------------------------------------
filetype on
set updatetime=5000
set nobackup
set noswapfile
set hidden
set bufhidden=hide
set noequalalways
let mapleader=" "
let maplocalleader=","
set autochdir
set mouse=a
set noimdisable
set undofile
if empty($TMPDIR)
	set undodir=/tmp/vim/undo
else
	set undodir=$TMPDIR/vim/undo
endif
set scrolloff=10
set spellsuggest=best,10
set fileencodings=ucs-bom,utf-8,utf-16,latin1

" Visuals --------------------------------------------------------------
syntax on
set t_Co=256
set number
" set relativenumber
set lazyredraw
set cursorline
set wrap
set linebreak
set showmode
set showcmd
set wildmenu
set autoindent
set breakindent
set smarttab
set tabstop=2
set shiftwidth=2
set softtabstop=0
set noexpandtab
set smartindent
set foldmethod=marker
set list
set listchars=eol:¬,tab:»-,space:·,trail:~,extends:>,precedes:<
set conceallevel=2
set termguicolors
set cmdheight=0

" Search ---------------------------------------------------------------
set ignorecase
set smartcase
set incsearch
set hlsearch
nnoremap <silent> <Esc> :noh<Cr>

" Remaps ---------------------------------------------------------------
" load vimrc
nnoremap <F2> :e $MYVIMRC<Cr>

" insert mode move
inoremap <A-h> <Left>
inoremap <A-j> <Down>
inoremap <A-k> <Up>
inoremap <A-l> <Right>

" move lines
nnoremap <A-k> ddkP
nnoremap <A-j> ddp
nnoremap <A-h> 0
nnoremap <A-l> $
xnoremap <A-k> dkP`[V`]
xnoremap <A-j> dp`[V`]
xnoremap <A-h> <gv
xnoremap <A-l> >gv

" switch buffer
nnoremap <Leader><Tab> <C-^>

" system yank
nnoremap Y "+Y
xnoremap Y "+y
xnoremap X "+x

" quick command
nnoremap ; :
xnoremap ; :

" delete words
inoremap <C-u> <C-g>u<C-u>
inoremap <C-w> <C-g>u<C-w>

" find the wrong word before and correct it
inoremap <C-s> <C-g>u<Esc>[s1z=`]a<C-g>u

" exit terminal
tnoremap <A-Esc> <C-\><C-n>

" delete tailing spaces
nnoremap <Leader>, :%s/\s\+$//e<Cr>

" line helper
nmap <Leader>. x$a<C-u><Esc>70p<C-c>f<space>l

" quick replace (multi cursor like feature)
xnoremap <Leader>r :s/<C-r>"//g<Left><Left>

" helix like mapping
nnoremap gl $
nnoremap gh 0
nnoremap <A-x> V
xnoremap <A-x> j
xnoremap <C-x> k
nnoremap <A-.> >>
nnoremap <A-,> <<
xnoremap <A-.> >gv
xnoremap <A-,> <gv

" Languages -----------------------------------------------------------
let g:python_recommended_style = 0
let g:rust_recommended_style = 0
let g:markdown_recommended_style = 0
let g:meson_recommended_style = 0
let fortran_free_source = 1
let fortran_have_tabs = 1
let fortran_more_precise = 1
let fortran_do_enddo = 1
let g:julia_set_indentation = 0

" Commands -------------------------------------------------------------
" delete all buffer except current
:command! BdOther silent! execute "%bd|e#|bd#"

" Tweaks ---------------------------------------------------------------
" auto-generate non-existing directories on save
function s:MkNonExDir(file, buf)
	if empty(getbufvar(a:buf, '&buftype')) && a:file!~#'\v^\w+\:\/'
		let dir=fnamemodify(a:file, ':h')
		if !isdirectory(dir)
			call mkdir(dir, 'p')
		endif
	endif
endfunction
augroup BWCCreateDir
	autocmd!
	autocmd BufWritePre * :call s:MkNonExDir(expand('<afile>'), +expand('<abuf>'))
augroup END

" highlight yanked lines
augroup highlight_yank
	autocmd!
	autocmd TextYankPost * silent! lua vim.highlight.on_yank({higroup="IncSearch", timeout=300})
augroup END

" never insert line as comment when using `o` to enter insert mode
augroup no_comment_on_newline
	autocmd!
	autocmd BufRead,BufNewFile * setlocal formatoptions-=o
augroup END

augroup spell_check
	autocmd!
	autocmd FileType gitcommit,markdown,tex,text setlocal spell
augroup END

augroup comment_styles
	autocmd!
	autocmd FileType cs,jsonc,c,cpp setlocal commentstring=//%s
	autocmd FileType matlab setlocal commentstring=%%s
	autocmd FileType qmake setlocal commentstring=#%s
augroup END

augroup extension_filetype
	autocmd!
	autocmd BufRead,BufNewFile *.qrc set filetype=xml
	autocmd BufRead,BufNewFile *.pro set filetype=qmake
	autocmd BufRead,BufNewFile *.F90 set filetype=fortran
	autocmd BufRead,BufNewFile *.astro set filetype=astro
augroup END

" ======================================================================
"}}}

" Plugins{{{
" ======================================================================
" loading plugins
if has('unix')
	source $HOME/.config/nvim/vimscript/plugins-conf.vim
else
	source $HOME/AppData/Local/nvim/vimscript/plugins-conf.vim
endif
lua require('plugins')
"}}}
