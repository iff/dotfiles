#! /bin/bash
paru -S --needed \
noto-fonts \
noto-fonts-cjk \
noto-fonts-emoji \
ttf-fira-code \
ttf-nerd-fonts-symbols-mono \
\
papirus-icon-theme \
arc-gtk-theme \
kvantum \
bibata-cursor-theme \
\
dbus-python \
fcitx5-im \
fcitx5-gtk \
fcitx5-qt \
fcitx5-lua \
fcitx5-material-color \
fcitx5-mozc \
fcitx5-chinese-addons \
\
firefox-developer-edition \
yazi \
mpv \
yt-dlp \
ristretto \
zathura \
zathura-pdf-poppler \
poppler \
poppler-data \
poppler-glib \

