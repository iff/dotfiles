#! /bin/bash
if [[ $# -eq 0 ]]; then
	echo "Specify a subdirectory in the home folder to link with."
	exit 1
fi

echo
echo "Linking to $1"
echo "====================================================================="

if [[ ! -d "/home/$USER/$1" ]]; then
	echo "Specify an existing subdirectoly in the home folder to link with."
	exit 1
fi

declare -a arr=("Templates" "Notes" "Records")

for i in "${arr[@]}"; do
	if [[ ! -d "/home/$USER/Documents/$i" ]]; then
		sudo ln -s /home/$USER/$1/$i ~/Documents/
	else
		echo "$i directory already exists."
fi
done

echo
echo "Linking spell files"
echo "====================================================================="

mkdir -p "/home/$USER/.config/nvim/spell"
for entry in "/home/$USER/Documents/Records/Spell"/* ; do
	sudo ln -s -f "$entry" "/home/$USER/.config/nvim/spell/"
done

exit 0
