(custom-set-variables
 '(package-selected-packages
	 '(one-themes org-bullets which-key undo-tree evil dashboard markdown-mode yasnippet-snippets yasnippet company use-package company-quickhelp)))
(custom-set-faces
 )

(setq inhibit-startup-message t)
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(setq make-backup-files nil)

(setq ring-bell-function 'ignore)
(global-hl-line-mode 1)

(fset 'yes-or-no-p 'y-or-n-p)

(global-display-line-numbers-mode 1)
(setq column-number-mode t)

(setq-default indent-tabs-mode t)
(setq-default tab-width 2)

(show-paren-mode t)

(mouse-avoidance-mode 'animate)

(setq show-paren-style 'parenthesis)

(setq frame-title-format "emacs@%b")

(defun open-init-file()
  (interactive)
  (find-file "~/.config/emacs/init.el"))

(global-set-key (kbd "<f2>") 'open-init-file)

; (setq indo-enable-flex-matching t)
; (setq ido-everywhere t)
; (ido-mode 1)

(require 'recentf)
(recentf-mode 1)
(setq recentf-max-menu-item 10)

(global-set-key (kbd "C-x C-r") 'recentf-open-files)
(set-language-environment "UTF-8")

;; loading packages
(require 'package)
(setq package-enable-at-start nil)
(add-to-list
 'package-archives
 '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
	(package-refresh-contents)
	(package-install 'use-package))

(use-package evil
  :ensure t
	:custom
	(evil-want-C-u-scroll t)
  :config
  (evil-mode 1)
  (evil-set-leader 'normal (kbd "SPC"))
  (evil-define-key 'normal 'global
    (kbd "gc") 'comment-dwim
    (kbd ";") 'evil-ex) 
  (evil-define-key 'normal 'org-mode-map
    (kbd "<leader>lp") 'org-latex-preview)
	)

(use-package undo-tree
  :ensure t
  :config
  (global-undo-tree-mode)
  (evil-set-undo-system 'undo-tree))

(use-package which-key
  :ensure t
  :config
  (which-key-mode))

;; Org-mode stuff
(use-package org-bullets
  :ensure t
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

;; (defalias 'list-buffers 'ibuffer)
;; (defalias 'list-buffers 'ibuffer-other-window)

(use-package company
  :ensure t
	:custom
	(company-minimum-prefix-length 1)
	(company-idle-delay 0)
  (company-selection-wrap-around t)
  :config
  (add-hook 
    'after-init-hook 'global-company-mode
    (add-to-list 'company-backends 'company-yasnippet)
    ))

(use-package company-quickhelp
	:ensure t
	:config
	(company-quickhelp-mode))

(use-package one-themes
  :ensure t
  :config
  (load-theme 'one-light t))

(use-package yasnippet
  :ensure t
  :config
  (yas-global-mode 1))

(use-package yasnippet-snippets
  :ensure t)

(use-package markdown-mode
  :ensure t
  :config
  (setq markdown-enable-math t)
  :mode
  (("README\\.md\\'" . gfm-mode)
   ("\\.md\\'" . markdown-mode)
   ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))

