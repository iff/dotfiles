#! /bin/bash

SUDO=sudo

echo
echo "coping files"
echo "====================================================================="
$SUDO mv -u /etc/X11/xorg.conf.d/* $PWD/xorg.conf.d/

echo
echo "linking"
echo "====================================================================="

$SUDO ln -s -n -f $PWD/pam_environment/environment /etc/environment
ln -s -n -f $PWD/shell ~/.shell
ln -s -n -f $PWD/zsh/.zprofile ~/
ln -s -n -f $PWD/zsh/.zshrc ~/
ln -s -n -f $PWD/fish/ ~/.config/
ln -s -n -f $PWD/nushell/ ~/.config/
ln -s -n -f $PWD/tmux/.tmux.conf ~/
ln -s -n -f $PWD/zellij/ ~/.config/
ln -s -n -f $PWD/starship/starship.toml ~/.config/
ln -s -n -f $PWD/paru ~/.config/
ln -s -n -f $PWD/foot ~/.config/
ln -s -n -f $PWD/alacritty/alacritty.toml ~/.config/
ln -s -n -f $PWD/kitty ~/.config/
ln -s -n -f $PWD/wezterm ~/.config/
ln -s -n -f $PWD/nvim ~/.config/
ln -s -n -f $PWD/helix ~/.config/
ln -s -n -f $PWD/emacs ~/.config/
ln -s -n -f $PWD/sway ~/.config/
ln -s -n -f $PWD/waybar ~/.config/
ln -s -n -f $PWD/i3 ~/.config/
ln -s -n -f $PWD/i3status ~/.config/
ln -s -n -f $PWD/i3blocks ~/.config/
ln -s -n -f $PWD/awesome ~/.config/
ln -s -n -f $PWD/leftwm ~/.config/
ln -s -n -f $PWD/polybar ~/.config/
ln -s -n -f $PWD/xinit/.xinitrc ~/
ln -s -n -f $PWD/rofi ~/.config/
ln -s -n -f $PWD/dunst ~/.config/
ln -s -n -f $PWD/imwheel/.imwheelrc ~/
ln -s -n -f $PWD/libinput-gestures/libinput-gestures.conf ~/.config/
ln -s -n -f $PWD/fcitx5 ~/.config/
ln -s -n -f $PWD/zathura ~/.config/
$SUDO rm -rf /etc/X11/xorg.conf.d
$SUDO ln -s -n -f $PWD/xorg.conf.d/ /etc/X11/

echo
echo "changing permissions"
echo "====================================================================="
chmod u+x $PWD/shell/*
chmod u+x $PWD/leftwm/up $PWD/leftwm/down

echo
echo "setting shell"
echo "====================================================================="
echo "select the shell you want to use"
echo "1) zsh"
echo "2) fish"
read -p "Enter your choice: " choice
case $choice in
	1) $SUDO pacman -S --noconfirm --needed zsh starship; chsh -s /bin/zsh;;
	2) $SUDO pacman -S --noconfirm --needed fish fisher starship;
		fish -c 'fisher install jorgebucaran/fisher';
		fish -c 'fisher install 0rax/fish-bd';
		chsh -s /bin/fish;;
	*) echo "Skip setting shell";;
esac

echo
echo "installing GUI session"
echo "====================================================================="
echo "select the GUI session you want to use (empty to skip)"
echo "1) sway"
echo "2) i3"
echo "3) awesome"
echo "4) leftwm"
read -p "Enter your choice: " choice
case $choice in
	1) $SUDO pacman -S --noconfirm --needed sway swaylock swaybg swaync autotiling-rs swayidle waybar wofi wl-clipboard;;
	2) $SUDO pacman -S --noconfirm --needed i3-gaps i3status i3blocks;;
	3) $SUDO pacman -S --noconfirm --needed awesome;;
	4) $SUDO pacman -S --noconfirm --needed leftwm;;
	*) echo "Skip installing GUI session";;
esac

echo
read -p "install misc packages? (y/N): " choice
case $choice in
	y|Y) echo "installing misc packages";
	echo "=====================================================================";
	$SUDO pacman -S --noconfirm --needed neovim python-pynvim;
	$SUDO pacman -S --noconfirm --needed alsa-utils alsa-plugins pulseaudio pavucontrol;
	$SUDO pacman -S --noconfirm --needed zoxide;;
	*) echo "Skip installing misc packages";;
esac
