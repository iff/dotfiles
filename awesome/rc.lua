-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

local volume_widget = require('awesome-wm-widgets.volume-widget.volume')
local cpu_widget = require("awesome-wm-widgets.cpu-widget.cpu-widget")
local ram_widget = require("awesome-wm-widgets.ram-widget.ram-widget")
local calendar_widget = require("awesome-wm-widgets.calendar-widget.calendar")

-- getting weather data
-- 1st line: api
-- 2nd and 3rd line: coordinates
local f = io.open(os.getenv("HOME") .. '/.config/awesome/weather_data', "r")
local weather_data = nil
if f ~=nil then
	weather_data = {}
	for line in io.lines( os.getenv("HOME") .. '/.config/awesome/weather_data') do
		weather_data[#weather_data + 1] = line
	end
	io.close(f)
end


-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
	naughty.notify({ preset = naughty.config.presets.critical,
		title = "Oops, there were errors during startup!",
		text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
	local in_error = false
	awesome.connect_signal("debug::error", function (err)
		-- Make sure we don't go into an endless error loop
		if in_error then return end
		in_error = true

		naughty.notify({ preset = naughty.config.presets.critical,
			title = "Oops, an error happened!",
			text = tostring(err) })
		in_error = false
	end)
end
-- }}}

-- {{{ Variable definitions
beautiful.init(string.format("%s/.config/awesome/themes/theme.lua", os.getenv("HOME")))

terminal = os.getenv("TERMINAL") or "alacritty"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor

modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
	awful.layout.suit.tile,
	-- awful.layout.suit.tile.left,
	awful.layout.suit.tile.bottom,
	-- awful.layout.suit.tile.top,
	awful.layout.suit.fair,
	awful.layout.suit.fair.horizontal,
	-- awful.layout.suit.spiral,
	awful.layout.suit.spiral.dwindle,
	awful.layout.suit.max,
	awful.layout.suit.max.fullscreen,
	awful.layout.suit.magnifier,
	awful.layout.suit.corner.nw,
	-- awful.layout.suit.corner.ne,
	-- awful.layout.suit.corner.sw,
	-- awful.layout.suit.corner.se,
	awful.layout.suit.floating,
}
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
	{ "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
	{ "manual", terminal .. " -e man awesome" },
	{ "edit config", editor_cmd .. " " .. awesome.conffile },
	{ "restart", awesome.restart },
	{ "quit", function() awesome.quit() end },
}

mysystemmenu = {
	{ "suspend", "systemctl suspend-then-hibernate" },
	{ "hibernate", "systemctl hibernate" },
	{ "hib. & reb.", "systemctl hybrid-sleep" },
	{ "reboot", "systemctl reboot" },
	{ "poweroff", "systemctl poweroff -i" },
}

mysystemmainmenu = awful.menu({ items = mysystemmenu })

mymainmenu = awful.menu({ items = {
	{ "awesome", myawesomemenu, beautiful.awesome_icon },
	-- { "open terminal", terminal }
	{ "system", mysystemmenu }
}
})

-- mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- {{{ Wibar
mytextclock = wibox.widget.textclock("%a, %b %d, %I:%M %p")
local cw = calendar_widget({
	radius = 0,
	previous_month_button = 4,
	next_month_button = 5,
})
mytextclock:connect_signal("button::press",
	function(_, _, _, button)
		if button == 1 then cw.toggle() end
	end)

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
	awful.button({ }, 1, function(t) t:view_only() end),
	awful.button({ modkey }, 1, function(t)
		if client.focus then
			client.focus:move_to_tag(t)
		end
	end),
	awful.button({ }, 3, awful.tag.viewtoggle),
	awful.button({ modkey }, 3, function(t)
		if client.focus then
			client.focus:toggle_tag(t)
		end
	end),
	awful.button({ }, 4, function(t) awful.tag.viewprev(t.screen) end),
	awful.button({ }, 5, function(t) awful.tag.viewnext(t.screen) end)
)

local tasklist_buttons = gears.table.join(
	awful.button({ }, 1, function (c)
		if c == client.focus then
			c.minimized = true
		else
			c:emit_signal(
				"request::activate",
				"tasklist",
				{raise = true}
			)
		end
	end),
	awful.button({ }, 3, function()
		awful.menu.client_list({ theme = { width = 250 } })
	end),
	awful.button({ }, 4, function ()
		awful.client.focus.byidx(-1)
	end),
	awful.button({ }, 5, function ()
		awful.client.focus.byidx(1)
	end))

local function set_wallpaper(s)
	-- Wallpaper
	if beautiful.wallpaper then
		local wallpaper = beautiful.wallpaper
		-- If wallpaper is a function, call it with the screen
		if type(wallpaper) == "function" then
			wallpaper = wallpaper(s)
		end
		gears.wallpaper.maximized(wallpaper, s)
	end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

-- wibox widgets
center_widgets = {
				mytextclock,
				layout = wibox.layout.fixed.horizontal,
				spacing = 5,
}
if weather_data then
	local weather_widget = require("awesome-wm-widgets.weather-widget.weather")
	table.insert(center_widgets,
		weather_widget({
			api_key=weather_data[1],
			coordinates = {weather_data[2], weather_data[3]},
			show_hourly_forecast = true,
			show_daily_forecast = true,
			timeout = 900,
		})
	)
end

right_widgets = {
	layout = wibox.layout.fixed.horizontal,
	spacing = 5,
	{
		layout = wibox.container.margin,
		top = 2,
		bottom = 2,
		wibox.widget.systray{
			set_reverse = true,
		},
	},
	ram_widget{
		timeout = 10,
	},
	cpu_widget{
		timeout = 10
	},
	volume_widget{
		widget_type = 'arc',
		thickness = 1.5,
		mute_color = beautiful.bg_urgent,
		with_icion = false,
	},
}

if os.execute("command -v xbacklight") ~= nil then
	local brightness_widget = require("awesome-wm-widgets.brightness-widget.brightness")
	table.insert(right_widgets, 
		brightness_widget{
			type = 'arc',
			program = 'xbacklight',
			step = 5,
			timeout = 1;
		}
	)
end
if os.execute("command -v acpi") ~= nil then
	local batteryarc_widget = require("awesome-wm-widgets.batteryarc-widget.batteryarc")
	table.insert(right_widgets, 
		batteryarc_widget({
			show_current_level = true,
			arc_thickness = 1.5,
			warning_msg_position = 'top_right',
			timeout = 10,
		})
	)
end

awful.screen.connect_for_each_screen(function(s)
	-- Wallpaper
	set_wallpaper(s)

	-- Each screen has its own tag table.
	awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }, s, awful.layout.layouts[1])

	-- Create a promptbox for each screen
	s.mypromptbox = awful.widget.prompt()
	-- Create an imagebox widget which will contain an icon indicating which layout we're using.
	-- We need one layoutbox per screen.
	s.mylayoutbox = awful.widget.layoutbox(s)
	s.mylayoutbox:buttons(gears.table.join(
		awful.button({ }, 1, function () awful.layout.inc( 1) end),
		awful.button({ }, 3, function () awful.layout.inc(-1) end),
		awful.button({ }, 4, function () awful.layout.inc(-1) end),
		awful.button({ }, 5, function () awful.layout.inc( 1) end)))
	-- Create a taglist widget
	s.mytaglist = awful.widget.taglist {
		screen  = s,
		filter  = awful.widget.taglist.filter.all,
		buttons = taglist_buttons
	}

	-- Create a tasklist widget
	s.mytasklist = awful.widget.tasklist {
		screen  = s,
		filter  = awful.widget.tasklist.filter.currenttags,
		buttons = tasklist_buttons,
		widget_template = {
			{
				{
					{
						{
							id     = 'icon_role',
							widget = wibox.widget.imagebox,
						},
						widget  = wibox.container.margin,
						top = 2,
						bottom = 2,
						right = 5,
						lerft = 5,
					},
					{
						id     = 'text_role',
						widget = wibox.widget.textbox,
					},
					layout = wibox.layout.fixed.horizontal,
				},
				widget = wibox.container.margin,
				left  = 5,
				right = 5,
			},
			id     = 'background_role',
			widget = wibox.container.background,
		},
	}

	-- Create the wibox
	s.mywibox = awful.wibar({ position = "top", screen = s, height = 20 })

	-- Add widgets to the wibox
	s.mywibox:setup {
		layout = wibox.layout.align.horizontal,
		expand = 'outside',
		{ -- Left widgets
			layout = wibox.layout.fixed.horizontal,
			s.mytaglist,
			s.mypromptbox,
			s.mytasklist,
		},
		{ -- Center widgets
			layout = wibox.container.margin,
			left = 10,
			right = 10,
			center_widgets,
		},
		{ -- Right widgets
			layout = wibox.container.place,
			halign = 'right',
			right_widgets,
			table.insert(right_widgets, s.mylayoutbox)
		},
	}
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
	awful.button({ }, 3, function () mymainmenu:toggle() end),
	awful.button({ }, 4, awful.tag.viewprev),
	awful.button({ }, 5, awful.tag.viewnext)
))
-- }}}

-- {{{ Key bindings
globalkeys = gears.table.join(
	awful.key({ modkey, }, "s",	  hotkeys_popup.show_help,
		{description="show help", group = "awesome"}),
	awful.key({ modkey, }, "Left",   awful.tag.viewprev,
		{description = "view previous", group = "tag"}),
	awful.key({ modkey, }, "Right",  awful.tag.viewnext,
		{description = "view next", group = "tag"}),
	awful.key({ modkey, }, "Escape", awful.tag.history.restore,
		{description = "go back", group = "tag"}),

	awful.key({ modkey, }, "j",
		function ()
			awful.client.focus.byidx( 1)
		end,
		{description = "focus next by index", group = "client"}
	),
	awful.key({ modkey, }, "k",
		function ()
			awful.client.focus.byidx(-1)
		end,
		{description = "focus previous by index", group = "client"}
	),

	-- menus
	awful.key({ modkey, }, "w", function () mymainmenu:show() end,
		{description = "show main menu", group = "awesome"}),
	awful.key({ modkey, "Shift" }, "n", function () mysystemmainmenu:show() end,
		{description = "show system power menu", group = "awesome"}),

	-- Layout manipulation
	awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1) end,
		{description = "swap with next client by index", group = "client"}),
	awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1) end,
		{description = "swap with previous client by index", group = "client"}),
	awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
		{description = "focus the next screen", group = "screen"}),
	awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
		{description = "focus the previous screen", group = "screen"}),
	awful.key({ modkey, }, "u", awful.client.urgent.jumpto,
		{description = "jump to urgent client", group = "client"}),
	awful.key({ modkey, }, "Tab",
		function ()
			awful.client.focus.history.previous()
			if client.focus then
				client.focus:raise()
			end
		end,
		{description = "go back", group = "client"}),
	awful.key({ modkey, "Shift" }, "Tab",
		function ()
			awful.menu.client_list({ theme = {width = 250 } })
		end,
		{description = "client list", group = "client"}
	),

	-- multimedia keys
	awful.key({ }, "XF86AudioRaiseVolume", function () awful.spawn('pactl set-sink-volume @DEFAULT_SINK@ +1%') end,
		{description = "increase volume", group = "multimedia"}),
	awful.key({ }, "XF86AudioLowerVolume", function () awful.spawn('pactl set-sink-volume @DEFAULT_SINK@ -1%') end,
		{description = "decrease volume", group = "multimedia"}),
	awful.key({ }, "XF86AudioMute", function () awful.spawn('pactl set-sink-mute @DEFAULT_SINK@ toggle') end,
		{description = "audio mute", group = "multimedia"}),
	awful.key({ }, "XF86AudioMicMute", function () awful.spawn('pactl set-source-mute @DEFAULT_SOURCE@ toggle') end,
		{description = "mic mute", group = "multimedia"}),

	awful.key({ }, "XF86AudioPlay", function () awful.spawn('playerctl play') end,
		{description = "play", group = "multimedia"}),
	awful.key({ }, "XF86AudioPause", function () awful.spawn('playerctl pause') end,
		{description = "pause", group = "multimedia"}),
	awful.key({ }, "XF86AudioNext", function () awful.spawn('playerctl next') end,
		{description = "next", group = "multimedia"}),
	awful.key({ }, "XF86AudioPrev", function () awful.spawn('playerctl previous') end,
		{description = "previous", group = "multimedia"}),

	awful.key({ }, "XF86MonBrightnessUp", function () awful.spawn('xbacklight -inc 1') end,
		{description = "brightness up", group = "screen"}),
	awful.key({ }, "XF86MonBrightnessDown", function () awful.spawn('xbacklight -dec 1') end,
		{description = "brightness down", group = "screen"}),

	-- Commands
	awful.key({ modkey }, "z", function () awful.spawn.with_shell('sleep 1 && xset dpms force off') end,
		{description = "turn off screen and lock", group = "screen"}),
	awful.key({ modkey }, "]", function () awful.spawn.with_shell('setxkbmap -layout us -option') end,
		{description = "change layout to qwerty", group = "screen"}),
	awful.key({ modkey }, "[", function () awful.spawn.with_shell('setxkbmap -layout us -variant colemak -option caps:swapescape') end,
		{description = "change layout to colemak", group = "screen"}),

	-- Standard program
	awful.key({ modkey, }, "Return", function () awful.spawn(terminal) end,
		{description = "open a terminal", group = "launcher"}),
	awful.key({ modkey, "Ctrl" }, "r", awesome.restart,
		{description = "reload awesome", group = "awesome"}),
	awful.key({ modkey, "Shift" }, "s", function () awful.spawn('flameshot gui') end,
		{description = "flameshot screenhot", group = "applications"}),
	awful.key({ modkey }, "q", function () awful.spawn('firefox-developer-edition') end,
		{description = "firefox-developer-edition", group = "applications"}),
	awful.key({ modkey, "Ctrl" }, "q", function () awful.spawn('firefox-developer-edition --private-window') end,
		{description = "firefox-developer-edition -private browsing-", group = "applications"}),

	awful.key({ modkey, }, "l", function () awful.tag.incmwfact( 0.05) end,
		{description = "increase master width factor", group = "layout"}),
	awful.key({ modkey, }, "h", function () awful.tag.incmwfact(-0.05) end,
		{description = "decrease master width factor", group = "layout"}),
	awful.key({ modkey, "Shift" }, "h",	 function () awful.tag.incnmaster( 1, nil, true) end,
		{description = "increase the number of master clients", group = "layout"}),
	awful.key({ modkey, "Shift" }, "l",	 function () awful.tag.incnmaster(-1, nil, true) end,
		{description = "decrease the number of master clients", group = "layout"}),
	awful.key({ modkey, "Control" }, "h",	 function () awful.tag.incncol( 1, nil, true) end,
		{description = "increase the number of columns", group = "layout"}),
	awful.key({ modkey, "Control" }, "l",	 function () awful.tag.incncol(-1, nil, true) end,
		{description = "decrease the number of columns", group = "layout"}),
	awful.key({ modkey, "Shift" }, "space", function () awful.layout.inc(1) end,
		{description = "select next", group = "layout"}),
	awful.key({ modkey, "Control" }, "space", function () awful.layout.inc(-1) end,
		{description = "select previous", group = "layout"}),

	awful.key({ modkey, "Control" }, "n",
		function ()
			local c = awful.client.restore()
			-- Focus restored client
			if c then
				c:emit_signal(
					"request::activate", "key.unminimize", {raise = true}
				)
			end
		end,
		{description = "restore minimized", group = "client"}),

	-- Prompt
	awful.key({ modkey }, "r", function () awful.screen.focused().mypromptbox:run() end,
		{description = "run prompt", group = "launcher"}),

	awful.key({ modkey }, "x",
		function ()
			awful.prompt.run {
				prompt	   = "Run Lua code: ",
				textbox	  = awful.screen.focused().mypromptbox.widget,
				exe_callback = awful.util.eval,
				history_path = awful.util.get_cache_dir() .. "/history_eval"
			}
		end,
		{description = "lua execute prompt", group = "awesome"}),
	-- Menubar
	awful.key({ modkey }, "d", function() menubar.show() end,
		{description = "show the menubar", group = "launcher"}),
	-- Notifications
	awful.key({ modkey }, "`", function() naughty.destroy_all_notifications() end,
		{description = "clear notifications", group = "awesome"})
)

clientkeys = gears.table.join(
	awful.key({ modkey, }, "f",
		function (c)
			c.fullscreen = not c.fullscreen
			c:raise()
		end,
		{description = "toggle fullscreen", group = "client"}),
	awful.key({ modkey, "Shift" }, "q", function (c) c:kill() end,
		{description = "close", group = "client"}),
	awful.key({ modkey, }, "space", awful.client.floating.toggle ,
		{description = "toggle floating", group = "client"}),
	awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
		{description = "move to master", group = "client"}),
	awful.key({ modkey, }, "o", function (c) c:move_to_screen() end,
		{description = "move to screen", group = "client"}),
	awful.key({ modkey, }, "t", function (c) c.ontop = not c.ontop end,
		{description = "toggle keep on top", group = "client"}),
	awful.key({ modkey, }, "n",
		function (c)
			c.minimized = true
		end ,
		{description = "minimize", group = "client"}),
	awful.key({ modkey, }, "m",
		function (c)
			c.maximized = not c.maximized
			c:raise()
		end ,
		{description = "(un)maximize", group = "client"}),
	awful.key({ modkey, "Control" }, "m",
		function (c)
			c.maximized_vertical = not c.maximized_vertical
			c:raise()
		end ,
		{description = "(un)maximize vertically", group = "client"}),
	awful.key({ modkey, "Shift" }, "m",
		function (c)
			c.maximized_horizontal = not c.maximized_horizontal
			c:raise()
		end ,
		{description = "(un)maximize horizontally", group = "client"})
)

-- Bind all key numbers to tags.
for i = 1, 12 do
	globalkeys = gears.table.join(globalkeys,
		-- View tag only.
		awful.key({ modkey }, "#" .. i + 9,
			function ()
				local screen = awful.screen.focused()
				local tag = screen.tags[i]
				if tag then
					tag:view_only()
				end
			end,
			{description = "view tag #"..i, group = "tag"}),
		-- Toggle tag display.
		awful.key({ modkey, "Control" }, "#" .. i + 9,
			function ()
				local screen = awful.screen.focused()
				local tag = screen.tags[i]
				if tag then
					awful.tag.viewtoggle(tag)
				end
			end,
			{description = "toggle tag #" .. i, group = "tag"}),
		-- Move client to tag.
		awful.key({ modkey, "Mod1" }, "#" .. i + 9,
			function ()
				if client.focus then
					local tag = client.focus.screen.tags[i]
					if tag then
						client.focus:move_to_tag(tag)
					end
				end
			end,
			{description = "move focused client to tag #"..i, group = "tag"}),
		-- Move and view client to tag.
		awful.key({ modkey, "Shift" }, "#" .. i + 9,
			function ()
				if client.focus then
					local tag = client.focus.screen.tags[i]
					if tag then
						client.focus:move_to_tag(tag)
					end
					local screen = awful.screen.focused()
					local tag = screen.tags[i]
					if tag then
						tag:view_only()
					end
				end
			end,
			{description = "move and view focused client to tag #"..i, group = "tag"}),
		-- Toggle tag on focused client.
		awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
			function ()
				if client.focus then
					local tag = client.focus.screen.tags[i]
					if tag then
						client.focus:toggle_tag(tag)
					end
				end
			end,
			{description = "toggle focused client on tag #" .. i, group = "tag"})
	)
end

clientbuttons = gears.table.join(
	awful.button({ }, 1, function (c)
		c:emit_signal("request::activate", "mouse_click", {raise = true})
	end),
	awful.button({ modkey }, 1, function (c)
		c:emit_signal("request::activate", "mouse_click", {raise = true})
		awful.mouse.client.move(c)
	end),
	awful.button({ modkey }, 3, function (c)
		c:emit_signal("request::activate", "mouse_click", {raise = true})
		awful.mouse.client.resize(c)
	end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
awful.rules.rules = {
	{ rule = { },
		properties = { border_width = beautiful.border_width,
			border_color = beautiful.border_normal,
			focus = awful.client.focus.filter,
			raise = true,
			keys = clientkeys,
			buttons = clientbuttons,
			screen = awful.screen.preferred,
			-- placement = awful.placement.no_overlap + awful.placement.no_offscreen
			placement = awful.placement.centered,
		},
		callback = awful.client.setslave
	},

	-- Borderless clients
	{ rule_any = {
		class = {
			"flameshot"
		}
	}, properties = { border_width = 0 }},

	-- Floating clients.
	{ rule_any = {
		instance = {
			"copyq",
		},
		class = {
			"Blueman-manager",
			"Pavucontrol",
			"flameshot",
			"^MATLAB.*",
			"icalingua"
		},

		name = {
			"Event Tester",  -- xev.
			"(DEBUG)$",
			"^Steam - News.*$",
			"^Friends List$"
		},
		role = {
			"AlarmWindow",  -- Thunderbird's calendar.
			"ConfigManager",  -- Thunderbird's about:config.
			"pop-up",	   -- e.g. Google Chrome's (detached) Developer Tools.
		}
	}, properties = { floating = true }},

	-- Set Firefox to always map on the tag named "2" on screen 1.
	-- { rule = { class = "Firefox" },
	--   properties = { screen = 1, tag = "2" } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
	-- Set the windows at the slave,
	-- i.e. put it at the end of others instead of setting it master.
	-- if not awesome.startup then awful.client.setslave(c) end

	if awesome.startup
		and not c.size_hints.user_position
		and not c.size_hints.program_position then
		-- Prevent clients from being unreachable after screen count changes.
		awful.placement.no_offscreen(c)
	end
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
	c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

-- notification{{{
naughty.config.defaults['icon_size'] = 50
naughty.config.defaults['max_width'] = 350
naughty.config.defaults['max_height'] = 200
--}}}

-- Autostart {{{
awful.spawn.with_shell(
	'if (xrdb -query | grep -q "^awesome\\.started:\\s*true$"); then exit; fi;' ..
	'xrdb -merge <<< "awesome.started:true";' ..
	-- listing
	'xss-lock --transfer-sleep-lock -- $HOME/.shell/lock &;' ..
	'flameshot &;' ..
	-- 'picom &;' ..
	'nm-applet &;' ..
	'fcitx5 -d &;' ..
	'/usr/lib/pam_kwallet_init &;' ..
	'kDrive &;'
)
--- }}}

gears.timer {
	timeout = 30,
	autostart = true,
	callback = function() collectgarbage() end
}

