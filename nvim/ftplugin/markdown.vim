nmap <buffer><silent> <A-p> :call mdip#MarkdownClipboardImage()<CR>
highlight Conceal ctermfg=33 guifg=#4078f2
nnoremap <buffer><silent> <leader>t :TableModeRealign<CR>

let b:coc_pairs = [["$", "$"]]
let b:coc_pairs_disabled = ["<"]

if has("win32")
	:command! -buffer MDpdf cd %:p:h | AsyncRun pandoc %:t -H P:\\Templates\\md2pdfHeader.tex --template=P:\\Templates\\md2pdftemplate.tex --pdf-engine=xelatex --number-sections --toc --citeproc -o %:r.pdf
	:command! -buffer MDpdfDefaults cd %:p:h | AsyncRun pandoc -d defaults.yaml %:t -H P:\\Templates\\md2pdfHeader.tex --template=P:\\Templates\\md2pdftemplate.tex --pdf-engine=xelatex --number-sections --toc --citeproc -o %:r.pdf
	:command! -buffer MDpdfPrint cd %:p:h | AsyncRun pandoc %:t -H P:\\Templates\\md2pdfHeaderPrint.tex --template=P:\\Templates\\md2pdftemplate.tex --pdf-engine=xelatex --number-sections --citeproc -o %:rPrint.pdf
	:command! -buffer MDpdfHand cd %:p:h | AsyncRun pandoc %:t -H P:\\Templates\\md2pdfHeaderHand.tex --template=P:\\Templates\\md2pdftemplate.tex --pdf-engine=xelatex --number-sections --citeproc -M fontsize:12pt -o %:rHand.pdf
	cabbrev <buffer> mdpdf cd %:p:h | AsyncRun pandoc %:t -H P:\\Templates\\md2pdfHeader.tex --template=P:\\Templates\\md2pdftemplate.tex --pdf-engine=xelatex --number-sections --toc --citeproc -o %:r.pdf
else
	:command! -buffer MDpdf cd %:p:h | AsyncRun pandoc %:t -H $HOME/Documents/Templates/md2pdfHeader.tex --template=$HOME/Documents/Templates/md2pdftemplate.tex --pdf-engine=xelatex --number-sections --toc --citeproc -o %:r.pdf
	:command! -buffer MDpdfDefaults cd %:p:h | AsyncRun pandoc -d defaults.yaml %:t -H $HOME/Documents/Templates/md2pdfHeader.tex --template=$HOME/Documents/Templates/md2pdftemplate.tex --pdf-engine=xelatex --number-sections --toc --citeproc -o %:r.pdf
	:command! -buffer MDpdfPrint cd %:p:h | AsyncRun pandoc %:t -H $HOME/Documents/Templates/md2pdfHeaderPrint.tex --template=$HOME/Documents/Templates/md2pdftemplate.tex --pdf-engine=xelatex --number-sections --citeproc -o %:rPrint.pdf
	:command! -buffer MDpdfHand cd %:p:h | AsyncRun pandoc %:t -H $HOME/Documents/Templates/md2pdfHeaderHand.tex --template=$HOME/Documents/Templates/md2pdftemplate.tex --pdf-engine=xelatex --number-sections --citeproc -M fontsize:12pt -o %:rHand.pdf
	cabbrev <buffer> mdpdf cd %:p:h \| AsyncRun pandoc %:t -o %:r.pdf -H $HOME/Documents/Templates/md2pdfHeader.tex --template=$HOME/Documents/Templates/md2pdftemplate.tex --pdf-engine=xelatex --number-sections --toc --citeproc -o %:r.pdf
endif

