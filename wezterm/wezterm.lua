local wezterm = require 'wezterm'

local config = {}

if wezterm.config_builder then
	config = wezterm.config_builder()
end

config.term = 'wezterm'

config.font = wezterm.font_with_fallback({
	'Iosevka Term',
	'DejaVu Sans Mono',
	'Fira Code',
	'Symbols Nerd Font Mono',
})
config.font_size = 9
config.color_scheme = 'Catppuccin Latte (Gogh)'
config.enable_tab_bar = false

config.window_close_confirmation = 'NeverPrompt'
config.warn_about_missing_glyphs = false

config.window_padding = {
	left = 0,
	right = 0,
	top = 0,
	bottom = 0,
}

return config

