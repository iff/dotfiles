if [ -f ~/.extra_source ]; then
	source ~/.extra_source
fi

if [ -z $DISPLAY ] && [ -z $WAYLAND_DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
	XDG_CURRENT_DESKTOP=sway exec sway
	return
fi
