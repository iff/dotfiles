rmdir /s "%HOMEDRIVE%%HOMEPATH%\AppData\Local\nvim"
mklink /d "%HOMEDRIVE%%HOMEPATH%\AppData\Local\nvim" "%HOMEDRIVE%%HOMEPATH%\Code\dotfiles\nvim"

rmdir /s "%HOMEDRIVE%%HOMEPATH%\AppData\Roaming\.emacs.d"
mklink /d "%HOMEDRIVE%%HOMEPATH%\AppData\Roaming\.emacs.d" "%HOMEDRIVE%%HOMEPATH%\Code\dotfiles\emacs\.emacs.d"

del "%HOMEDRIVE%%HOMEPATH%\AppData\Local\Microsoft\Windows Terminal\settings.json"
mklink "%HOMEDRIVE%%HOMEPATH%\AppData\Local\Microsoft\Windows Terminal\settings.json" "%HOMEDRIVE%%HOMEPATH%\Code\dotfiles\windows-terminal\settings.json"

