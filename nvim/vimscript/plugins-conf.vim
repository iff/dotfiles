" Color schemes --------------------------------------------------------
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set termguicolors
if (has('unix') && empty($DISPLAY) && empty($WAYLAND_DISPLAY))
	set background=dark
else
	set background=light
endif

" " coc.nvim -------------------------------------------------------------{{{
" nmap <silent> gd <Plug>(coc-definition)
" nmap <silent> gy <Plug>(coc-type-definition)
" nmap <silent> gi <Plug>(coc-implementation)
" nmap <silent> gr <Plug>(coc-references)
" nmap <silent> [g <Plug>(coc-diagnostic-prev)
" nmap <silent> ]g <Plug>(coc-diagnostic-next)
" nmap <Leader>rn <Plug>(coc-rename)
" nmap <Leader>ac  <Plug>(coc-codeaction)
" xmap <leader>ac  <Plug>(coc-codeaction-selected)
" nmap <Leader>cl  <Plug>(coc-codelens-action)
" nmap <Leader>qf  <Plug>(coc-fix-current)
" autocmd CursorHold * silent call CocActionAsync('highlight')

" let g:coc_global_extensions = [
" 			\ 'coc-explorer',
" 			\ 'coc-snippets',
" 			\ 'coc-highlight',
" 			\ 'coc-yank',
" 			\ 'coc-git',
" 			\ ]

" " tab behavior
" inoremap <silent><expr> <TAB>
" 			\ coc#pum#visible() ? coc#_select_confirm() :
" 			\ coc#expandable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand',''])\<CR>" :
" 			\ <SID>check_back_space() ? "\<Tab>" :
" 			\ coc#refresh()

" imap <M-e> <Plug>(coc-snippets-expand)
" inoremap <silent><expr> <M-r> coc#refresh()

" function! s:check_back_space() abort
" 	let col = col('.') - 1
" 	return !col || getline('.')[col - 1] =~# '\s'
" endfunction

" xmap <tab> <Plug>(coc-snippets-select)

" " multi-cursor
" nmap <expr> <silent> <C-s> <SID>select_current_word()
" nmap <silent> <A-s> <Plug>(coc-cursors-word)
" function! s:select_current_word()
" 	if !get(b:, 'coc_cursors_activated', 0)
" 		return "\<Plug>(coc-cursors-word)*\<Plug>(coc-cursors-word):nohlsearch\<Cr>"
" 	endif
" 	return "*\<Plug>(coc-cursors-word):nohlsearch\<Cr>"
" endfunc
" highlight link CocCursorRange Visual
" highlight link CocMenuSel CursorLine

" " show documentation
" nnoremap <silent> <Leader>d :call <SID>show_documentation()<Cr>
" " and signature
" inoremap <silent> <A-d> <C-o>:call CocActionAsync('showSignatureHelp')<Cr>

" function! s:show_documentation()
" 	if (index(['vim','help'], &filetype) >= 0)
" 		execute 'h '.expand('<cword>')
" 	elseif (coc#rpc#ready())
" 		call CocActionAsync('doHover')
" 	else
" 		execute '!' . &keywordprg . " " . expand('<cword>')
" 	endif
" endfunction

" " diagnostic
" nmap <silent> [g <Plug>(coc-diagnostic-prev)
" nmap <silent> ]g <Plug>(coc-diagnostic-next)

" " float scrolling
" nnoremap <silent><nowait><expr> <PageDown> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
" nnoremap <silent><nowait><expr> <PageUp> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
" inoremap <silent><nowait><expr> <PageDown> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
" inoremap <silent><nowait><expr> <PageUp> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
" vnoremap <silent><nowait><expr> <PageDown> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
" vnoremap <silent><nowait><expr> <PageUp> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"

" " coc-snippet
" inoremap <A-n> <nop>
" inoremap <A-p> <nop>
" let g:coc_snippet_next = '<A-n>'
" let g:coc_snippet_prev = '<A-p>'

" " coc-pairs
" inoremap <silent><expr> <Cr>
" 			\ "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" " coc-yank
" nnoremap <silent> <Leader>y :<C-u>CocList -A --normal yank<cr>

" " coc-explorer
" nnoremap <silent> <C-b> :CocCommand explorer
" 			\ --toggle
" 			\ --sources=buffer+,file+
" 			\ <Cr>

" nnoremap <silent> <expr> <M-b> ExploreCurrentBufferDir() . "\<Cr>"

" function! ExploreCurrentBufferDir()
" 	return ":CocCommand explorer
" 				\ --toggle
" 				\ --sources=buffer+,file+
" 				\ ". expand('%:p:h')
" endfunction

" autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe "norm \<C-b>" | wincmd p | bd! | ene | exe "cd ".argv()[0] | endif
" "}}}

" wilder.nvim ----------------------------------------------------------
" call wilder#setup({'modes': [':', '/', '?']})
" call wilder#set_option('pipeline', [
" 			\ 	wilder#branch(
" 			\ 		wilder#cmdline_pipeline(),
" 			\ 		wilder#search_pipeline(),
" 			\ 	),
" 			\ ])

" call wilder#set_option('renderer', wilder#wildmenu_renderer({
"       \ 'highlighter': wilder#basic_highlighter(),
" 			\ }))

" call wilder#set_option('renderer', wilder#popupmenu_renderer({
" 			\ 'highlighter': wilder#basic_highlighter(),
" 			\ 'highlights': {
" 			\ 	'accent': wilder#make_hl('WilderAccent', 'Pmenu', [{}, {}, {'foreground': '#f4468f'}]),
" 			\ },
" 			\ 'left': [
" 			\ 	' ', wilder#popupmenu_devicons(),
" 			\ ],
" 			\ 'right': [
" 			\ 	' ', wilder#popupmenu_scrollbar(),
" 			\ ],
" 			\ }))

" Telescope ------------------------------------------------------------
nnoremap <silent> <Leader>f :Telescope find_files<Cr>
nnoremap <silent> <Leader>b :Telescope buffers<Cr>
nnoremap <silent> <Leader>d :Telescope diagnostic<Cr>
nnoremap <silent> <Leader>l :Telescope current_buffer_fuzzy_find<Cr>
nnoremap <silent> <Leader>L :Telescope live_grep<Cr>
nnoremap <silent> <Leader>h :Telescope oldfiles<Cr>
nnoremap <silent> <Leader>m :Telescope marks<Cr>
nnoremap <silent> <Leader>s :Telescope lsp_document_symbols<Cr>
nnoremap <silent> <Leader>S :Telescope lsp_dynamic_workspace_symbols<Cr>
nnoremap <silent> <Leader>t :Telescope tags<Cr>
nnoremap <silent> gr :Telescope lsp_references<Cr>
nnoremap <silent> <Leader>p :Telescope projects<CR>

" Restore view ---------------------------------------------------------
" set viewoptions=cursor,folds,slash,unix

" Markdown -------------------------------------------------------------
let g:vim_markdown_strikethrough = 1
let g:vim_markdown_math = 1
let g:vim_markdown_follow_anchor = 1
let g:vim_markdown_new_list_item_indent = 0

" markdown preview
let g:mkdp_refresh_slow = 1
let g:mkdp_auto_start = 0
let g:mkdp_auto_close = 0

let g:mkdp_preview_options = {
			\ 'mkit': {},
			\ 'katex': {
				\ 'macros': {
					\ "\\sech": "\\operatorname{sech}",
					\ "\\dd": "\\mathop{}\\!\\mathrm{d}",
					\ "\\DD": "\\mathop{}\\!\\mathrm{D}",
					\ "\\xint": "\\times\\mkern-20mu\\int",
					\ "\\celsius": "\\degree\\mathrm{C}",
					\	'\\mathbf': '\\pmb{\\mathrm{#1}}',
					\ "\\iu": "\\mathrm{i}"
				\ }
			\ },
			\ 'uml': {},
			\ 'maid': {},
			\ 'disable_sync_scroll': 0,
			\ 'sync_scroll_type': 'middle',
			\ 'hide_yaml_meta': 1,
			\ 'sequence_diagrams': {},
			\ 'flowchart_diagrams': {},
			\ 'content_editable': v:false,
			\ 'disable_filename': 1
			\ }

" misc ------------------------------------------------------------------
let g:slime_target = "neovim"
let g:slime_no_mappings = 1
