local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		'git',
		'clone',
		'--filter=blob:none',
		'https://github.com/folke/lazy.nvim.git',
		'--branch=stable', -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

local config = require("plugins-conf")

local plugins = {
	-- core
	{
		'nvim-treesitter/nvim-treesitter',
		build = ':TSUpdate',
		-- event = 'VeryLazy',
		dependencies = {
			{
				'nvim-treesitter/nvim-treesitter-context',
				config = config['treesitter-context'],
			},
		},
		config = config['treesitter'],
	},

	-- coding ---------------------------------------------------------------
	-- {'neoclide/coc.nvim',-- {{{
	-- 	branch = 'release',
	-- event = 'InsertEnter',
	-- 	dependencies = 'honza/vim-snippets'
	-- }-- }}}


	{
		'hrsh7th/nvim-cmp', -- {{{
		event = 'InsertEnter',
		dependencies = {
			'hrsh7th/cmp-nvim-lsp',
			'hrsh7th/cmp-buffer',
			'hrsh7th/cmp-path',
			'hrsh7th/cmp-cmdline',
			'petertriho/cmp-git',
			'SirVer/ultisnips',
			'quangnguyen30192/cmp-nvim-ultisnips',
			'honza/vim-snippets',
		},
		config = config['cmp'],
	},
	-- }}}

	{
		'neovim/nvim-lspconfig',
		-- event = 'VeryLazy',
		dependencies = {
			'onsails/lspkind.nvim',
			{
				'lvimuser/lsp-inlayhints.nvim',
				config = config['inlayhints'],
			},
		},
		config = config['lspconfig'],
	},


	{
		'lewis6991/gitsigns.nvim',
		event = 'VeryLazy',
		config = config['git-signs'],
	},
	{
		'RRethy/vim-illuminate',
		event = 'VeryLazy',
		config = config['illuminate'],
	},
	{
		'windwp/nvim-autopairs',
		event = 'InsertEnter',
		config = config['autopairs'],
	},
	{
		'numToStr/Comment.nvim',
		dependencies = 'JoosepAlviste/nvim-ts-context-commentstring',
		keys = {
			{ '<C-c>', mode = { 'n', 'v' } },
			{ '<A-c>', mode = { 'n', 'v' } }
		},
		config = config['comment'],
	},
	{
		'ur4ltz/surround.nvim',
		keys = {
			{ 'cs' },
			{ 'ds' },
			{ 's', mode = { 'v' } }
		},
		config = config['surround'],
	},

	-- look and feels -------------------------------------------------------
	{
		'nvim-lualine/lualine.nvim',
		event = 'VeryLazy',
		dependencies = { 'kyazdani42/nvim-web-devicons' },
		config = config['lualine'],
	},
	{
		'b0o/incline.nvim',
		event = 'WinNew',
		config = config['incline'],
	},
	{
		'catppuccin/nvim',
		name = 'catppuccin',
		config = config['catppuccin'],
	},
	{
		'anuvyklack/pretty-fold.nvim',
		event = 'VeryLazy',
		dependencies = { 'anuvyklack/fold-preview.nvim', 'anuvyklack/nvim-keymap-amend' }
	},
	{
		'norcalli/nvim-colorizer.lua',
		cmd = 'ColorizerToggle',
		config = config['colorizer']
	},

	-- markdown -------------------------------------------------------------
	{
		'preservim/vim-markdown',
		build = ':call mkdp#util#install()',
		ft = 'markdown',
		dependencies = {
			{
				'ferrine/md-img-paste.vim',
				keys = '<A-p>'
			},
			{
				'iamcco/markdown-preview.nvim',
				cmd = 'MarkdownPreview'
			},
			{
				'dhruvasagar/vim-table-mode',
				lazy = true,
			}
		},
	},

	-- languages ------------------------------------------------------------
	{
		'https://codeberg.org/iff/qalculate.vim.git',
		ft = 'qalculate',
	},

	-- quality of life ------------------------------------------------------
	{
		'nvim-telescope/telescope.nvim',
		dependencies = {
			'nvim-lua/plenary.nvim',
			'ahmedkhalf/project.nvim',
		},
		event = 'VeryLazy',
		config = config['telescope'],
	},
	{
		'thalesmello/tabfold',
		keys = {'<Tab>', mode = 'n'},
	},
	{
		'vim-scripts/restore_view.vim',
		event = 'BufRead'
	},
	{
		'lukas-reineke/indent-blankline.nvim',
		main = 'ibl',
		-- event = 'VeryLazy',
		config = config['indent-blankline'],
	},
	{
		'lambdalisue/suda.vim',
		cmd = 'SudaWrite',
	},
	{
		'zbirenbaum/copilot.lua',
		event = 'InsertEnter',
		config = config['copilot'],
	},
	-- conflicts with tabfold
	-- {
	-- 	'folke/which-key.nvim',
	-- 	event = 'VeryLazy',
	-- 	config = true
	-- },
	{
		'Vonr/align.nvim',
		branch = "v2",
		lazy = true,
		init = config['align']
	},
	{
		'tpope/vim-sleuth',
		event = 'BufRead',
	},

	-- etc ------------------------------------------------------------------
	{
		'skywind3000/asyncrun.vim',
		cmd = 'AsyncRun',
	},
	{
		'jpalardy/vim-slime',
		keys = { '<F7>' },
		config = config['slime'],
	},
	{
		'phaazon/hop.nvim',
		keys = { '<Leader>w' },
		config = config['hop'],
	},
	{
		'akinsho/git-conflict.nvim',
		event = 'VeryLazy',
		version = '*',
		config = true
	},
}

require('conf')
require('lazy').setup({
	plugins,
})
