# dotfiles

This repository contains my configuration files.

**DO NOT** execute any of the scripts before you read its content. `set.sh` will DELETE your own configuration files and replace them by the content of this repository with symbolic links.
