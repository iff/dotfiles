# zsh config{{{
# ======================================================================

bindkey -v

# history
# ----------------------------------------------------------------------
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt extended_history
setopt hist_expire_dups_first
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_verify
setopt share_history

# compleation
# ----------------------------------------------------------------------
autoload -Uz compinit
compinit
eval "$(dircolors)"
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'
zstyle ':completion:*:*:*:*:processes' command "ps -u $USERNAME -o pid,user,comm -w -w"
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-zA-Z-_}={A-Za-z_-}' 'r:|=*' 'l:|=* r:|=*'
zstyle ':completion::complete:*' gain-privileges 1
zstyle ':completion:*' special-dirs true
zstyle ':completion:*' verbose true
zstyle ':completion:*' group-name ''
zstyle ':completion:*' list-dirs-first true
zstyle ':completion:*' accept-exact-dirs true
setopt COMPLETE_ALIASES
setopt auto_menu
setopt complete_in_word
setopt always_to_end
autoload -U +X bashcompinit && bashcompinit
bindkey '^[[Z' reverse-menu-complete

# directories
# ----------------------------------------------------------------------
setopt auto_pushd
setopt pushd_ignore_dups
setopt pushdminus
autoload -U colors && colors
export CLICOLOR=1
export LSCOLORS=ExGxBxDxCxEgEdxbxgxcxd
WORDCHARS=''

# env
# ----------------------------------------------------------------------
export PATH=$PATH:\
~/.cargo/bin:\
~/.shell


# alias
# ----------------------------------------------------------------------
# ls
if [[ -x "$(command -v eza)" ]]; then
	alias ls="eza --group-directories-first"
else
	alias ls="ls --color=auto --group-directories-first"
fi
alias l="ls -lhF"
alias la="ls -lAhF"
alias lt="ls -ThF"


# grep
alias grep="grep --color=auto"

if [[ $TERM == "xterm-kitty" ]]; then
	alias diff="kitty +kitten diff"
	alias icat="kitty +kitten icat"
else
	alias diff="diff --color"
fi

# math
alias m="qalc"

# shortcuts
alias n="$FILEMANAGER"
o(){xdg-open "$1" &>/dev/null & disown}
mnv() mpv --no-video $*
alias mnv="noglob mnv"
alias mt="trash"
alias cr="sudo sh -c 'sync; echo 3 > /proc/sys/vm/drop_caches'"

# misc
eval "$(zoxide init zsh)"
eval "$(starship init zsh)"
alias ms="noglob ms"
source <(fzf --zsh)

eval "$(~/Code/pay-respects/target/release/pay-respects zsh --alias)"

# zinit{{{
# ======================================================================

# zinit installer
# ----------------------------------------------------------------------
if [[ ! -f $HOME/.zinit/bin/zinit.zsh ]]; then
	print -P "%F{33}▓▒░ %F{220}Installing %F{33}DHARMA%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
	command mkdir -p "$HOME/.zinit" && command chmod g-rwX "$HOME/.zinit"
	command git clone https://github.com/zdharma-continuum/zinit "$HOME/.zinit/bin" && \
		print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" || \
		print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi

# load zinit
# ----------------------------------------------------------------------
source "$HOME/.zinit/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit
unalias zi # alias conflict

# load plugins
# ----------------------------------------------------------------------
zinit ice depth=1
zinit light-mode for \
	zdharma-continuum/zinit-annex-readurl \
	zdharma-continuum/zinit-annex-bin-gem-node \
	zdharma-continuum/zinit-annex-patch-dl \
	zdharma-continuum/zinit-annex-rust \
	zdharma-continuum/fast-syntax-highlighting \
	Tarrasch/zsh-bd \
	zsh-users/zsh-completions \
	zsh-users/zsh-autosuggestions \
	zsh-users/zsh-history-substring-search \

# plugin configs
# ----------------------------------------------------------------------
# substring-search
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
HISTORY_SUBSTRING_SEARCH_FUZZY=1
HISTORY_SUBSTRING_SEARCH_ENSURE_UNIQUE=1
# }}}

if [[ -z "$TMUX" ]]; then
tmux -2 new-session -A -s main 2> /dev/null
fi
# if [[ -z "$ZELLIJ" ]]; then
# 	zellij attach -c main
# fi
